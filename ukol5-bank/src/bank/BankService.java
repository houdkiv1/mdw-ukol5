package bank;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

@WebService
public class BankService {
	ArrayList<BankAccount> list = new ArrayList<BankAccount>() {
	    {
	        add(new BankAccount(123, 2000));
	        add(new BankAccount(456, -8000));
	        add(new BankAccount(789, 20000));
	        add(new BankAccount(888, 3000));
	        add(new BankAccount(999, 100000));
	        add(new BankAccount(015, -12000));
	    }
	};
	
	public Boolean validateAccount(int number){
		for(BankAccount b : list){
			if(b.getNumber() == number){
				return true;
			}
		}
		return false;
	}
	
	public Boolean validateBalance(int number){
		for(BankAccount b : list){
			if(b.getNumber() == number && b.getBalance() >=0){
				return true;
			}
		}
		return false;
	}
	
	public int changeBalance(int number, int amount){
		for(BankAccount b : list){
			if(b.getNumber() == number){
				b.changeBalance(amount);
				return b.getBalance();
			}
		}
		return 0;
	}
}
