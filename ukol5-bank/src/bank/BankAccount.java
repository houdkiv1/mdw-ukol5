package bank;

public class BankAccount {
	int number;
	int balance;
	
	BankAccount(int n, int b){
		number = n;
		balance = b;
	}
	
	int getNumber(){
		return number;
	}
	
	int getBalance(){
		return balance;
	}
	
	void changeBalance(int i){
		balance += i;
	}
}
