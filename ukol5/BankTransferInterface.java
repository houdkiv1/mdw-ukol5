package bank;

public interface BankTransferInterface{
	public Boolean transfer(int from, int to, int amount);
}
