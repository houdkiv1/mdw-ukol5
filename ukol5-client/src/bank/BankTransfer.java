package bank;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.jws.WebService;
import javax.xml.namespace.QName;

@WebService
public class BankTransfer{
	public Boolean transfer(int from, int to, int amount) throws RemoteException, MalformedURLException{	
		BankService serv = new BankServicePortBindingStub();
		System.out.println("volani stubu");
		if(!serv.validateAccount(from) || !serv.validateAccount(to)){
			return false;
		}
		System.out.println("ucty zkontrolovany");
		serv.changeBalance(from, -amount);
		serv.changeBalance(to, amount);
		
		return true;
	}
}
