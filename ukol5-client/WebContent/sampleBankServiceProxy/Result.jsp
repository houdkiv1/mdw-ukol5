<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="sampleBankServiceProxyid" scope="session" class="bank.BankServiceProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
sampleBankServiceProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = sampleBankServiceProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        sampleBankServiceProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        bank.BankService getBankService10mtemp = sampleBankServiceProxyid.getBankService();
if(getBankService10mtemp == null){
%>
<%=getBankService10mtemp %>
<%
}else{
        if(getBankService10mtemp!= null){
        String tempreturnp11 = getBankService10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String arg0_1id=  request.getParameter("arg016");
        int arg0_1idTemp  = Integer.parseInt(arg0_1id);
        String arg1_2id=  request.getParameter("arg118");
        int arg1_2idTemp  = Integer.parseInt(arg1_2id);
        int changeBalance13mtemp = sampleBankServiceProxyid.changeBalance(arg0_1idTemp,arg1_2idTemp);
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(changeBalance13mtemp));
        %>
        <%= tempResultreturnp14 %>
        <%
break;
case 20:
        gotMethod = true;
        String arg0_3id=  request.getParameter("arg023");
        int arg0_3idTemp  = Integer.parseInt(arg0_3id);
        java.lang.Boolean validateBalance20mtemp = sampleBankServiceProxyid.validateBalance(arg0_3idTemp);
if(validateBalance20mtemp == null){
%>
<%=validateBalance20mtemp %>
<%
}else{
        String tempResultreturnp21 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(validateBalance20mtemp));
        %>
        <%= tempResultreturnp21 %>
        <%
}
break;
case 25:
        gotMethod = true;
        String arg0_4id=  request.getParameter("arg028");
        int arg0_4idTemp  = Integer.parseInt(arg0_4id);
        java.lang.Boolean validateAccount25mtemp = sampleBankServiceProxyid.validateAccount(arg0_4idTemp);
if(validateAccount25mtemp == null){
%>
<%=validateAccount25mtemp %>
<%
}else{
        String tempResultreturnp26 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(validateAccount25mtemp));
        %>
        <%= tempResultreturnp26 %>
        <%
}
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>